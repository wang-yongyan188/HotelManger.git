/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : hotelsys

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 01/06/2022 14:12:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for roomtype
-- ----------------------------
DROP TABLE IF EXISTS `roomtype`;
CREATE TABLE `roomtype`  (
  `tid` int NOT NULL,
  `typename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `describe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `standardprice` decimal(8, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`tid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roomtype
-- ----------------------------
INSERT INTO `roomtype` VALUES (1, '单人间A款', '大床', 99.00);
INSERT INTO `roomtype` VALUES (2, '单人间B款', '环境好', 199.00);
INSERT INTO `roomtype` VALUES (3, '双人间A', NULL, 109.00);
INSERT INTO `roomtype` VALUES (4, '双人间B', NULL, 299.00);
INSERT INTO `roomtype` VALUES (5, '三人间', NULL, 399.00);
INSERT INTO `roomtype` VALUES (6, '钟点房', '2小时', 100.00);

SET FOREIGN_KEY_CHECKS = 1;
