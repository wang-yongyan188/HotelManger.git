/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : hotelsys

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 01/06/2022 14:11:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for myorder
-- ----------------------------
DROP TABLE IF EXISTS `myorder`;
CREATE TABLE `myorder`  (
  `oid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'uuid',
  `starttime` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '入住时间',
  `endtime` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '截止时间',
  `realtime` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '实际离开时间',
  `rid` int NOT NULL COMMENT '客房编号',
  `cid` bigint NOT NULL COMMENT '登记顾客编号',
  `eid` int NOT NULL COMMENT '办理工作人员id',
  `cidrest` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '其他入住人id',
  `realprice` decimal(8, 2) NOT NULL COMMENT '实际支付',
  `status` int NOT NULL DEFAULT 0 COMMENT '1有效 0失效',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`oid`) USING BTREE,
  INDEX `eid`(`eid`) USING BTREE,
  INDEX `rid`(`rid`) USING BTREE,
  INDEX `cid`(`cid`) USING BTREE,
  CONSTRAINT `cid` FOREIGN KEY (`cid`) REFERENCES `customer` (`cid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of myorder
-- ----------------------------
INSERT INTO `myorder` VALUES ('21e7415521f04377b7d853ab80e3ac61', '2022-05-31 22:19:26', '2022-06-01 12:00:00', '2022-06-01 10:02:53', 101, 220222, 1111, NULL, 98.10, 0, NULL);
INSERT INTO `myorder` VALUES ('eab071f941544af1bbc14b4825b17830', '2022-05-31 22:25:57', '2022-06-01 5:00:00', '2022-06-01 10:14:49', 102, 220222, 1111, NULL, 180.00, 0, '超时 -5');

SET FOREIGN_KEY_CHECKS = 1;
