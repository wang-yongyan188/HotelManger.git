package com.yan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotelMangerApplication {
    public static void main(String[] args) {
        SpringApplication.run(HotelMangerApplication.class,args);
    }
}
