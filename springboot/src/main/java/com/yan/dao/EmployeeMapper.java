package com.yan.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yan.entity.Customer;
import com.yan.entity.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {
   Employee getAllbyIdAndName(@Param("id") int id,@Param("name")String name);
   List<Employee>getAllEms();

}
