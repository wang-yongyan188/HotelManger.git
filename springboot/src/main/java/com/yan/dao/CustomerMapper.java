package com.yan.dao;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yan.entity.Customer;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CustomerMapper extends BaseMapper<Customer> {
    public List<Customer>getAllCus();
    public List<Customer> getIsDel();

}
