package com.yan.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yan.entity.Room;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface RoomMapper extends BaseMapper<Room> {
    public List<Room> getAllRoom();
    public List<Room>getAllFullRoom();
    public List<Room>getAllRemainRoom();
    public List<Room>getAllStopRoom();
    public List<Room>getAllRoomByType(@Param("tid") int typeid);
    public List<Room>getAllRoomByAsc();
    public List<Room>getAllRoomByDesc();
    public Room getRoomById(int rid);
}
