package com.yan.dao;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yan.entity.MyOrder;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface OrderMapper extends BaseMapper<MyOrder> {
    public int createOrder(MyOrder order);
    public List<MyOrder> getAllOrder();
    public List<MyOrder> getAllOrderByCid(Long cid);
}
