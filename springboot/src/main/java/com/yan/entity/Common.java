package com.yan.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
public class Common{
    private Integer status;
    private String msg;
    private Object data;
   public static Common error(int status,String msg){
        return new Common(status,msg,null);
    }
    public static Common success(String msg,Object data){
       return new Common(200,msg,data);
    }
    public static Common success(String msg){
        return new Common(200,msg,null);
    }
}
