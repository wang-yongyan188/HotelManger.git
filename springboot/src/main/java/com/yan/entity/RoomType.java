package com.yan.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@ToString
@Accessors(chain = true)
public class RoomType {
Integer tid;
String typename;
String describe;
Double standardprice;
}
