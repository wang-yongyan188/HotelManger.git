package com.yan.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
@TableName("myorder")
public class MyOrder {
    @TableId
    private String oid;

    @TableField("starttime")
    private String starttime;

    @TableField("endtime")
    private String endtime;

    @TableField("realtime")
    private String realtime;

    @TableField("rid")
    private Integer rid;

    @TableField("cid")
    private Long cid;

    @TableField("eid")
    private Integer eid;

    @TableField("cidrest")
    private Integer cidrest;

    @TableField("realprice")
    private Double realprice;

    @TableField("status")
    private Integer status;

    @TableField("remark")
    private String remark;

    @TableField(exist = false)
    private List<Customer>crest;
}
