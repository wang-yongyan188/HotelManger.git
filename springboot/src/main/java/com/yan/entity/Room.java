package com.yan.entity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName("room")
public class Room {
    @TableId
    private Integer rid;
    private Integer rtype;
    private Double extraprice;
    private Integer status;
    private String comment;
    @TableField(exist = false)
    private RoomType roomtype;
}
