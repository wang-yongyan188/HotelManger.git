package com.yan.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName("employee")
public class Employee {
    @TableId
    private Integer eid;
    private String name;
    private String passwd;
    private Integer status;
    private Integer counterid;
    private Integer ismanger;
}
