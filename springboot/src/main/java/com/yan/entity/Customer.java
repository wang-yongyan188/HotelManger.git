package com.yan.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
@TableName("customer")
public class Customer {
   @TableId
   private Long cid;
   private String name;
   private Long telnum;
   private Integer grade;
   private Integer isdel;
   @TableField(exist = false)
   private Grade grade1;

}