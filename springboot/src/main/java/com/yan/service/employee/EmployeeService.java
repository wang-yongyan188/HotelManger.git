package com.yan.service.employee;

import com.yan.entity.Common;
import com.yan.entity.Employee;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface EmployeeService {
   public Common login(Employee employee, HttpSession session);
   public List<Employee> getAllEms();
   public Boolean updateEms(Employee employee);
   public Common  isLoginByToken(String token,HttpSession session);
   public Common getEmsById(int eid);
   public List<Employee>getWorkingEmS();
   public List<Employee>getNoWorkEmS();
   public Common logout(String key,HttpSession session);
   public Common addEms(Employee employee);
   public Common delEms(int eid);
   public Boolean forceLeave(int eid,HttpSession session);
   public Common forceLogout(Employee employee,HttpSession session);
}
