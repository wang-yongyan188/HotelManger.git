package com.yan.service.customer;

import com.yan.entity.Common;
import com.yan.entity.Customer;

public interface CustomerService {
    public Common getAllCus();
    public Common getNoCus();
    public Common getIsDel();
    public Boolean RegCus(Customer customer);
    public Common delCus(int cid);
    public Common getCusByParam(String param,Long tel);
    public Common updateCus(Customer customer);
}
