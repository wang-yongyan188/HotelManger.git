package com.yan.service.customer;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yan.dao.CustomerMapper;
import com.yan.dao.GradeMapper;
import com.yan.entity.Common;
import com.yan.entity.Customer;
import com.yan.entity.Grade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CustomerServiceImp implements CustomerService {
    @Autowired
    CustomerMapper mapper;
    @Autowired
    GradeMapper gradeMapper;
    @Override
    public Common getAllCus() {
        try {
            List<Customer> allCus = mapper.getAllCus();
            return Common.success("查询成功",allCus);
        }catch (Exception e){
            return Common.error(500,"查询失败");
        }
    }
    @Override
    public Common getNoCus() {
        List<Customer> list = mapper.selectList(new QueryWrapper<Customer>().eq("isdel",0));
        return Common.success("查询成功",list);
    }

    @Override
    public Common getIsDel() {
        try{
            return Common.success("查询成功",mapper.selectList(new QueryWrapper<Customer>().eq("isdel",1)));
        }catch (Exception e){
            return Common.error(500,"查询失败");
        }
    }

    @Override
    public Boolean RegCus(Customer customer) {
        int insert = mapper.insert(customer);
        if(insert==1){
            return true;
        }
        return false;
    }

    @Override
    public Common delCus(int cid) {
        try{
            int i = mapper.deleteById(cid);
            if(i==0){
                return Common.success("删除成功");
            }
        }
        catch (Exception e){
            return Common.error(500,"删除出错");
        }
        return Common.error(500,"删除失败");
    }

    @Override
    public Common getCusByParam(String param,Long tel) {
        try{
            Customer one = mapper.selectOne(new QueryWrapper<Customer>().eq(param,tel));
            if(ObjectUtil.isNull(one)){
                return Common.error(500,"查询失败");
            }
            Integer grade = one.getGrade();
            Grade grade1 = gradeMapper.selectById(grade);
            one.setGrade1(grade1);

            return Common.success("查询成功",one);
        }
        catch (Exception e){
            return Common.error(500,"查询出错");
        }
    }

    @Override
    public Common updateCus(Customer customer) {
        try{
            return Common.success("修改成功",mapper.update(customer,new QueryWrapper<Customer>().eq("cid",customer.getCid())));
        }catch (Exception e){
            return Common.error(500,"修改出错");
        }
    }


}