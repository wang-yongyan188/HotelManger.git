package com.yan.service.room;
import com.yan.entity.Room;
import org.apache.ibatis.annotations.Param;

import java.util.List;
public interface RoomService{
    public List<Room> getAllRoom();
    public List<Room>getAllFullRoom();
    public List<Room>getAllRemainRoom();
    public List<Room>getAllStopRoom();
    public Integer addRoom(Room room);
    public Integer updateRoom(Room room);
    public Integer delRoom(Integer rid);
    public List<Room>getAllRoomByType(int typeid);
    public List<Room>getAllRoomByAsc();
    public List<Room>getAllRoomByDesc();
    public Room getRoomByRid(int rid);


}
