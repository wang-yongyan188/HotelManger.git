package com.yan.service.room;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yan.entity.Room;
import com.yan.dao.RoomMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class RoomServiceImp implements RoomService{
    @Autowired
    RoomMapper mapper;
    public List<Room>getAllRoom(){
       return mapper.getAllRoom();
    }

    public List<Room> getAllFullRoom() {
        return mapper.getAllFullRoom();
    }

    public List<Room> getAllRemainRoom() {

        return mapper.getAllRemainRoom();
    }

    public List<Room> getAllStopRoom() {
        return mapper.getAllStopRoom();
    }

    public Integer addRoom(Room room) {
        return mapper.insert(room);
    }

    @Override
    public Integer updateRoom(Room room) {
        QueryWrapper<Room> wrapper = new QueryWrapper<>();
        wrapper.eq("rid",room.getRid());
        boolean exists = mapper.exists(wrapper);
        if(exists){
            return  mapper.update(room,wrapper);
        }else
        {
            return 0;
        }
    }

    @Override
    public Integer delRoom(Integer rid) {
        return mapper.delete(new QueryWrapper<Room>().eq("rid",rid));
    }

    @Override
    public List<Room> getAllRoomByType(int typeid) {
        return mapper.getAllRoomByType(typeid);
    }

    @Override
    public List<Room> getAllRoomByAsc() {
        return mapper.getAllRoomByAsc();
    }

    @Override
    public List<Room> getAllRoomByDesc() {
        return  mapper.getAllRoomByDesc();
    }

    @Override
    public Room getRoomByRid(int rid) {
        return mapper.getRoomById(rid);
    }

}
