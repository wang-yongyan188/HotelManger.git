package com.yan.service.order;

import com.yan.entity.Common;
import com.yan.entity.MyOrder;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

public interface OrderService {
    public Common createOrder(Map map);
    public Common finishOrder(int rid,Long cid);
    public Common getOrderByCR(int rid,Long cid);
    public Common updateOrder(MyOrder myOrder);
    public Common getAllOrderByCid(Long cid);
    public Common getOrderByStatus(Long cid,int status);
    public Common getRunOrder(int i);
}
