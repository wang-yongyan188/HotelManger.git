package com.yan.control;
import com.yan.entity.Common;
import com.yan.service.order.OrderService;
import com.yan.service.order.OrderServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("order")
public class OrderController {
    @Autowired
    OrderServiceImp service;
    @PostMapping("/create")
    public Common createOrder(@RequestBody Map map){
      return service.createOrder(map);
    }


    @GetMapping("/getByCR/{rid}/{cid}")
    public Common getOrderByCr(@PathVariable("rid") int rid,@PathVariable("cid") Long cid){
        return service.getOrderByCR(rid,cid);
    }
    @GetMapping("/finish/{rid}/{cid}")
    public Common finishOrder(@PathVariable("rid") int rid,@PathVariable("cid") Long cid){
        return service.finishOrder(rid,cid);
    }
    @GetMapping("/getAllByCid/{cid}")
      public Common getAllOrderByCid(Long cid){
       return service.getAllOrderByCid(cid);
    }
    @GetMapping("/getRunOrder/{cid}")
    public Common getRunOrder(@PathVariable("cid") Long cid){
        return service.getOrderByStatus(cid,1);
    }
    @GetMapping("/getFinOrder/{cid}")
    public Common getFinOrder(@PathVariable("cid") Long cid){
        return service.getOrderByStatus(cid,0);
    }
    @GetMapping("/getRunOrder")
    public Common getRunOrder(){
        return service.getRunOrder(1);
    }
    @GetMapping("/getFinOrder")
    public Common getFinOrder(){
        return service.getRunOrder(0);
    }
}
