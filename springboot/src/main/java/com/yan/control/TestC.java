package com.yan.control;

import com.yan.dao.CustomerMapper;
import com.yan.dao.EmployeeMapper;
import com.yan.dao.OrderMapper;


import com.yan.entity.MyOrder;
import com.yan.service.employee.EmployeeServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("t")
public class TestC {
   @Autowired
    CustomerMapper mapper;
   @Autowired
    OrderMapper orderMapper;
   @GetMapping("/")
    public String getAll(){
       List<MyOrder> allOrder = orderMapper.getAllOrder();
       return allOrder.toString();

   }

}