package com.yan.control;
import com.yan.entity.Common;
import com.yan.entity.Employee;
import com.yan.service.employee.EmployeeServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.HandlerTypePredicate;

import javax.servlet.http.HttpSession;

@RestController
@CrossOrigin
@RequestMapping("ems")
public class EmployeeController {
    @Autowired
    EmployeeServiceImp service;
    @PostMapping("/login")
    public Common login(@RequestBody Employee employee, HttpSession session){
        try {
          return service.login(employee, session);
        }catch (Exception e){
            return Common.error(500,"登录失败");
        }
    }
    @PostMapping("isManger/{token}")
    public Boolean isManger(@PathVariable("token") String token,HttpSession session){
        Common ems = service.isLoginByToken(token,session);
        if(ems.getStatus()==200){
            Employee employee = (Employee) ems.getData();
            if(employee.getIsmanger()==1){
                return true;
            }
        }
        return false;
    }
    @PostMapping("getEmsByToken/{token}")
    public Common getEmsByToken(@PathVariable("token") String token,HttpSession session){
        return service.isLoginByToken(token,session);
    }
    @GetMapping("/logout/{token}")
    public Common logout(@PathVariable String token,HttpSession session){
        return service.logout(token,session);
    }
// 管理员调用
    @GetMapping("/getAll")
    public Common getAllEmployee(){
        try {
            return Common.success("查询成功",service.getAllEms());
        }catch (Exception e){
            return Common.error(500,"查询失败");
        }
    }
    @GetMapping("/getEmsById/{id}")
   public Common getEmsById(@PathVariable int id){
        return service.getEmsById(id);
    }

    @PostMapping("updateEms")
   public Common updateEms(@RequestBody Employee employee){
        Boolean updateEms = service.updateEms(employee);
        try {
            if(updateEms){
                return Common.success("修改成功",updateEms);
            }
        }catch (Exception e){
            return Common.error(500,"修改出错");
        }
        return Common.error(500,"修改失败");
    }

    @GetMapping("/getWorking")
    public Common getWorkingEmS(){
        try {
            return Common.success("查询成功", service.getWorkingEmS());
        }
        catch (Exception e){
            return Common.error(500,"查询失败");
        }
    }
    @GetMapping("/getNoWork")
    public Common getNOWorkEmS(){
        try {
            return Common.success("查询成功", service.getNoWorkEmS());
        }
        catch (Exception e){
            return Common.error(500,"查询失败");
        }
    }
    @PostMapping("/addEms")
    public Common addEms(@RequestBody Employee employee){
        return service.addEms(employee);
    }
    @DeleteMapping("/delEms/{eid}")
    public Common delEms(@PathVariable int eid){
        return service.delEms(eid);
    }
    @GetMapping("/forceLeave/{eid}")
    public Common forceLeave(@PathVariable int eid, HttpSession session){
        Boolean aBoolean = service.forceLeave(eid, session);
        if(aBoolean){
            return Common.success("该员工下线成功");
        }
        return Common.error(500,"退出失败");
    }
    @PostMapping("/forceLogout")
    public Common forceLogout(@RequestBody Employee employee,HttpSession session){
    return service.forceLogout(employee,session);

    }


}
