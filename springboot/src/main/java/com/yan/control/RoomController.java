package com.yan.control;
import com.yan.entity.Common;
import com.yan.entity.Room;
import com.yan.service.room.RoomServiceImp;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
@RestController
@CrossOrigin
@RequestMapping("room")
public class RoomController {
    @Autowired
    RoomServiceImp service;
    //查询
    @GetMapping("/getAll")
    public Common getAllRoom(){
        try{
            List<Room> allRoom = service.getAllRoom();
            return Common.success("查询成功",allRoom);
        }catch (Exception e){
            return Common.error(500,"查询失败");
        }
    }
    @GetMapping("/getAllFull")
    public Common getAllFull(){
        try {
            return Common.success("查询成功",service.getAllFullRoom());
        }catch (Exception e){
            return Common.error(500,"查询失败");
        }
    }
    @GetMapping("/getAllRemain")
    public Common getAllRemain(){
        try {
            return Common.success("查询成功",service.getAllRemainRoom());
        }catch (Exception e){
            return Common.error(500,"查询失败");
        }
    }
    @GetMapping("/getStop")
    public Common getStop(){
        try {
            return Common.success("查询成功",service.getAllStopRoom());
        }catch (Exception e){
            return Common.error(500,"查询失败");
        }
    }

//功能查询
@GetMapping("/byType/{tid}")
public Common getAllRoomByType(@PathVariable int tid){
    try {
        return Common.success("查询成功", service.getAllRoomByType(tid));
    }catch (Exception e){
        return Common.error(500,"查询失败");
    }
}
@GetMapping("getById/{rid}")
public Room getRoomById(@PathVariable int rid){
        return service.getRoomByRid(rid);
}
    @GetMapping("/asc")
    public Common getAllRoomByAsc(){
        try {
            return Common.success("查询成功", service.getAllRoomByAsc());
        }catch (Exception e){
            return Common.error(500,"查询失败");
        }
    }
    @GetMapping("/desc")
    public Common getAllRoomByDesc(){
        try {
            return Common.success("查询成功", service.getAllRoomByDesc());
        }catch (Exception e){
            return Common.error(500,"查询失败");
        }
    }
    //添加
    @PostMapping("/addRoom")
    public Common addRoom(@RequestBody Room room){
        try {
            Integer integer = service.addRoom(room);
            if(integer==1){
                return Common.success("添加成功");
            }
            else{
                throw new RuntimeException("添加错误");
            }
        }
        catch (Exception e){
            return Common.error(500,"添加失败");
        }
    }
    //修改
    @PostMapping("/updateRoom")
    public Common updateRoom(@RequestBody Room room){
        try {
            Integer integer = service.updateRoom(room);
            if(integer==1){
                return Common.success("添加成功");
            }
            else{
                throw new RuntimeException("添加错误");
            }
        }
        catch (Exception e){
            return Common.error(500,"添加失败");
        }
    }
    // 删除
    @DeleteMapping("/{rid}")
    public Common delRoom(@PathVariable("rid") int rid){
        try{
            Integer integer = service.delRoom(rid);
            if(integer==1){
                return Common.success("删除成功");
            }
            else{
                throw new RuntimeException("删除错误");
            }
         }catch (Exception e){
            return Common.error(500,"删除失败");
        }
    }
}
