package com.yan.control;
import com.yan.entity.Common;
import com.yan.entity.Customer;
import com.yan.service.customer.CustomerServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
@RestController
@CrossOrigin
@RequestMapping("cus")
public class CustomerController {
    @Autowired
    CustomerServiceImp service;

    @GetMapping("getAll")
    public Common getAllCus(){
        return service.getAllCus();
    }
    @GetMapping("getNoAll")
    public Common getNoAllCus(){
        return service.getNoCus();
    }

    @GetMapping("getIsDel")
    public Common getIsDel(){
        return service.getIsDel();
    }
    @GetMapping("getByTel/{tel}")
    public Common getCusByTel(@PathVariable Long tel){
        return service.getCusByParam("telnum",tel);
    }
    @GetMapping("getById/{id}")
    public Common getCusById(@PathVariable Long id){
        return service.getCusByParam("cid",id);
    }

    @PostMapping("regCus")
    public Common regCus(@RequestBody Customer customer){
        if( service.RegCus(customer)){
            return Common.success("注册成功");
        }
        return Common.error(500,"注册失败");
    }

    @DeleteMapping("delCus/{id}")
    public Common delCus(@PathVariable int id){
        return service.delCus(id);
    }

    @PostMapping("update")
    public Common updateCus(@RequestBody Customer customer){
        return service.updateCus(customer);
    }
}

