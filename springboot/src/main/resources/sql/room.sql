/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : hotelsys

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 01/06/2022 14:11:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for room
-- ----------------------------
DROP TABLE IF EXISTS `room`;
CREATE TABLE `room`  (
  `rid` int NOT NULL,
  `rtype` int NOT NULL,
  `extraprice` decimal(8, 2) NOT NULL DEFAULT 0.00 COMMENT '该类型的定价+-每个房间的上下价格浮动',
  `status` int NULL DEFAULT 0 COMMENT '0 没人 1 有人 2 暂定',
  `comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '额外说明',
  PRIMARY KEY (`rid`) USING BTREE,
  INDEX `type`(`rtype`) USING BTREE,
  CONSTRAINT `type` FOREIGN KEY (`rtype`) REFERENCES `roomtype` (`tid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of room
-- ----------------------------
INSERT INTO `room` VALUES (100, 1, -12.00, 0, NULL);
INSERT INTO `room` VALUES (101, 1, 10.00, 0, NULL);
INSERT INTO `room` VALUES (102, 2, 1.00, 0, NULL);
INSERT INTO `room` VALUES (103, 1, 0.00, 0, NULL);

SET FOREIGN_KEY_CHECKS = 1;
