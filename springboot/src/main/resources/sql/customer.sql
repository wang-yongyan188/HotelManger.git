/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : hotelsys

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 01/06/2022 14:11:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer`  (
  `cid` bigint NOT NULL AUTO_INCREMENT COMMENT '身份证号',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `telnum` bigint NOT NULL,
  `grade` int NULL DEFAULT NULL,
  `isdel` tinyint NULL DEFAULT 0 COMMENT '假删除',
  PRIMARY KEY (`cid`) USING BTREE,
  INDEX `grade`(`grade`) USING BTREE,
  CONSTRAINT `grade` FOREIGN KEY (`grade`) REFERENCES `grade` (`gradeid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 220285 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer
-- ----------------------------
INSERT INTO `customer` VALUES (220222, 'xaioming', 1514433, 3, 0);
INSERT INTO `customer` VALUES (220284, 'xaioxaio', 1514433, 3, 0);

SET FOREIGN_KEY_CHECKS = 1;
