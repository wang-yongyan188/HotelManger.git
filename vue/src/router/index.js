import Vue  from 'vue'
import VueRouter from 'vue-router'
import EmployeeLogin from "@/views/employee/EmployeeLogin";
import EmsHome from "@/views/employee/EmsHome";
import Room from "@/views/room/Room";
import Te from "@/views/room/Te";
import RegCus from "@/views/customer/RegCus";
import GetCus from "@/views/customer/GetCus";
import updateCus from "@/views/customer/updateCus";
import CreateOrder from "@/views/order/CreateOrder";
import FinishOrder from "@/views/order/FinishOrder";
import ShowOrder from "@/views/order/ShowOrder";
import getBycon from "@/views/order/getBycon";
import store from '@/store/index'
import MangerHome from "@/views/employee/MangerHome";
import DelEmsOrRoom from "@/views/employee/manger/DelEmsOrRoom";
Vue.use(VueRouter)
  const routes=[
    {path:'/emsLogin',name:'emsLogin',component:EmployeeLogin,meta:{checkLogin:false}},
    {path: '/' ,name: 'emsHome',component: EmsHome,meta:{checkLogin:true}},
    {path: '/roomShow',component: Room,meta:{checkLogin:true}},
    {path: '/t',component: Te},
    {path: '/regCus',component: RegCus,meta:{checkLogin:true}},
    {path: '/getCus',component: GetCus,meta:{checkLogin:true}},
    {path: '/updateCus',component: updateCus,meta:{checkLogin:true}},
    {path: '/createOrder',component: CreateOrder,meta:{checkLogin:true}},
    {path: '/finishOrder',component: FinishOrder,meta:{checkLogin:true}},
    {path: '/showOrder',component: ShowOrder,meta:{checkLogin:true}},
    {path: '/getByCon',component: getBycon,meta:{checkLogin:true}},
    {path: '/mangerHome',component: MangerHome,beforeEnter:(to,from,next)=>{
      if(store.state.isManger){
        alert("正在进入管理员操作")
        next();
      }else{
        next({path:'/emsLogin'})
      }

      }}

]
const router= new VueRouter({
    routes
})
router.beforeEach(((to, from, next) =>{

  if(to.meta.checkLogin){
    if(store.state.isEms){
      next();
    }
    else {
      next({path:'/emsLogin'})
    }

  }else{
    next();
  }
} ))


export default router