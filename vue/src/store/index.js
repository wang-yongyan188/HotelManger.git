import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        isEms:false,
        isManger:true
    },
    getters: {

    },
    mutations: {
        logined(state){
            state.isEms=true;
        },
        logouted(state){
            state.isEms=false;
        },
        isManger(state){
            state.isManger=true;
            state.isEms=false
        },
        NoManger(state){
            state.isManger=false;
        }
    },
    actions: {
    },
    modules: {
    }
})
